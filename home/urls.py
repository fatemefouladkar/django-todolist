from django.urls import path
from . import views
app_name='home'

urlpatterns =[
    path('',views.home,name='home'),
    path('add/',views.Add.as_view(),name='add'),
    path('list/',views.TaskList.as_view(),name='list'),
    path('list/<int:page>',views.TaskList.as_view(),name='list'),
    path('list/delete/<int:pk>',views.TaskDelete,name="delete"),
    path('list/undo-deletion/<int:pk>',views.TaskUndoDeletion,name="undo-deletion"),
    path('list/task-done/<int:pk>',views.TaskDone,name="task-done"),
    path('list/task-undone/<int:pk>',views.TaskUndone,name="task-done"),



]
