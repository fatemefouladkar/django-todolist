from django.contrib import admin
from django.contrib.admin.decorators import action
from . import models


# ----------------------- Actions ------------------------

@admin.action(description="undo soft deletion")
def undo_deletion(modeladmin,request,queryset):
	queryset.update(is_deleted=False)




# ----------------------- Registrations ------------------------

@admin.display(description='Deletion Time')
def delete_time(obj):
    if obj.is_deleted==True:
        return  obj.delete_time
    else:
        return "not deleted"


@admin.register(models.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display=('title','short_description','user','create_time',delete_time)
    actions = [undo_deletion]

    
    def short_description(self,obj):
        return obj.description[:50]

    short_description.short_description="Description"

