from django.http import Http404
from django.shortcuts import get_object_or_404,redirect
from .models import Task

class FormValidMixin():
	def form_valid(self,form):
		self.obj = form.save(commit=False)
		self.obj.user = self.request.user
		return super().form_valid(form)
