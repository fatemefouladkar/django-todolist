from django.contrib.messages.api import success
from django.db.models import fields, query
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render,HttpResponse,get_object_or_404
from django.views.generic import DetailView,ListView
from . import models
from django.urls import reverse,reverse_lazy
from django.contrib import messages
from django.views.generic.edit import CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from account.models import User
from django.contrib.messages.views import SuccessMessageMixin
from .mixins import FormValidMixin



# @login_required
# def add(request):
#     if request.method == "POST":
#         title=request.POST['title']
#         description=request.POST['description']
#         ins=models.Task(title=title,description=description)
#         print(title,description)
#         ins.save()
#         messages.success(request, 'Your New Task Was Saved')
#         return HttpResponseRedirect('/')
#     else:
#         return render(request,'home/add.html')


class Add(LoginRequiredMixin,SuccessMessageMixin,FormValidMixin,CreateView):
	model = models.Task
	template_name="home/add.html"
	fields=['title','description']
	success_message= 'Your New Task Was Saved'


def home(request):
	return render(request,'home/home.html')

class TaskList(LoginRequiredMixin, ListView):
	model = models.Task
	context_object_name="tasks"
	template_name = "home/list.html"
	paginate_by=5

	def get_queryset(self):
		return models.Task.objects.filter(user=self.request.user, is_deleted=False)


@login_required
def TaskDelete(request,pk):
	task=models.Task.objects.get(pk=pk)
	task.soft_delete()
	request.session['pk'] = task.pk
	messages.error(request, ('Task "%s" Was Deleted')%task.title )
	context={'pk':'pk'}
	return HttpResponseRedirect(reverse('home:list'))
	

@login_required   
def TaskUndoDeletion(request,pk):
	task=models.Task.objects.get(pk=pk)
	task.is_deleted=False
	task.save()
	return HttpResponseRedirect(reverse('home:list'))

def TaskDone(request,pk):
	task=models.Task.objects.get(pk=pk)
	task.status = 'd'
	task.save()
	return HttpResponseRedirect(reverse('home:list'))

def TaskUndone(request,pk):
	task=models.Task.objects.get(pk=pk)
	task.status = 'p'
	task.save()
	return HttpResponseRedirect(reverse('home:list'))