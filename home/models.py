from django.db import models
from django.utils import timezone
from account.models import User
from django.urls import reverse



class TaskManager(models.Manager):
    def available(self):
        return self.filter(is_deleted=False)


class Task(models.Model):
    STATUS_CHOICES = (
        ('d','done'),
        ('p','in progress'),
    )
    title=models.CharField(max_length=100)
    user =models.ForeignKey(User, null=True,on_delete=models.CASCADE,related_name='tasks',verbose_name='User')
    description=models.TextField(max_length=500)
    create_time=models.DateTimeField(auto_now_add=True,verbose_name='Creation Time')
    is_deleted=models.BooleanField(default=False)
    delete_time=models.DateTimeField(auto_now=True,verbose_name="Deletion Time")
    status = models.CharField(max_length=1,choices=STATUS_CHOICES, default='p')

    def get_absolute_url(self):
        return reverse("home:add")

    def __str__(self):
        return self.title

    def soft_delete(self):
        self.is_deleted=True
        self.save()
    
    class Meta:
        ordering=['-create_time']

    objects = TaskManager()