from django import template

register=template.Library()

@register.inclusion_tag('home/partials/link.html')
def link(request, link, content):
    return {
        "content":content,
        "request":request,
        "link":link,
    }


@register.simple_tag
def title(name="my site title"):
    return name 
