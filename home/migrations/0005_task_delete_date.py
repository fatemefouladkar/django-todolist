# Generated by Django 3.2.7 on 2021-10-14 17:53

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_task_is_deleted'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='delete_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
