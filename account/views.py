from django.shortcuts import render,redirect
from django.views.generic import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from account.models import User
from django.contrib.messages.views import SuccessMessageMixin
from home import models

# view for sign up 
from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from .forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from .models import User
from django.core.mail import EmailMessage

class Signup(CreateView):
        form_class = SignupForm
        template_name="registration/signup.html"
        def form_valid(self,form):
            user = form.save(commit=False)
            user.is_active = False       #user can't login without email confirmation
            user.save()
            current_site = get_current_site(self.request)
            mail_subject = 'Activate your blog account.'
            message = render_to_string('registration/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            email.send()
            return HttpResponse('Please confirm your email address to complete the registration')



def Activate(request, uidb64, token):   #check if token is valid ,then user gets activated and can login
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # login(request, user)      #user is logined immediately
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can <a href="/login">login</a> to your account.')
    else:
        return HttpResponse('Activation link is invalid!')




