from django.conf.urls import url
from django.urls.conf import path
from . import views


app_name = "account"

urlpatterns = [
    # signup & email confirmation
    path('signup/', views.Signup.as_view(), name='signup'),
    path('activate/<uidb64>/<token>', views.Activate, name='activate'),

]