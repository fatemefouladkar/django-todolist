### In order to run the app:
-------------
- virtualenv .venv

- source .venv/bin/activate (on linux)

- .venv/Scripts/Activate.ps1 (on windows)

- pip install -r requirements.txt

- pip install django-utils-six

- cp sample_.env .env (on linux - Change this file as you need)

- rename sample_.env to .env (on windows - Change this file as you need)

- ./manage.py runserver

- on browser -> http://localhost:8000/



